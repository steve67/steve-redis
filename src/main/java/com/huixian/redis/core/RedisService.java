package com.huixian.redis.core;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * create at  2018/08/10 15:11
 *
 * @author yanggang
 */
@Service
@Slf4j
public class RedisService {

    private static final String rateLimitScript = "local key = KEYS[1]\n" +
            "local limit = tonumber(ARGV[1])\n" +
            "local expire_time = ARGV[2]\n" +
            "local is_exists = redis.call(\"EXISTS\", key)\n" +
            "if is_exists == 1 then\n" +
            "    if redis.call(\"INCR\", key) > limit then\n" +
            "        return 0\n" +
            "    else\n" +
            "        return 1\n" +
            "    end\n" +
            "else\n" +
            "    redis.call(\"SET\", key, 1)\n" +
            "    redis.call(\"EXPIRE\", key, expire_time)\n" +
            "    return 1\n" +
            "end";
    private static final String countScript =   "local key = KEYS[1]\n" +
            "local expire_time = ARGV[1]\n" +
            "local step = ARGV[2]\n" +
            "local is_exists = redis.call(\"EXISTS\", key)\n" +
            "local result = redis.call(\"INCRBY\", key, step)\n" +
            "if is_exists == 0 then\n" +
            "   redis.call(\"EXPIRE\", key, expire_time)\n" +
            "end\n" +
            "return result\n";

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private JedisPool jedisPool;

    public void set(final String key, final Object value) {
        if (StringUtils.isBlank(key) || value == null) {
            log.error("参数异常,添加缓存失败");
            return;
        }
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.set(redisTemplate.getKeySerializer().serialize(key),
                    redisTemplate.getValueSerializer().serialize(value));
        } catch (Exception e) {
            log.error("添加缓存失败", e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }

    }

    public void setEX(final String key, final Object value, final int expirationSecond) {
        if (StringUtils.isBlank(key) || value == null) {
            log.error("参数异常,添加缓存失败");
            return;
        }
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.setex(redisTemplate.getKeySerializer().serialize(key), expirationSecond,
                    redisTemplate.getValueSerializer().serialize(value));
        } catch (Exception e) {
            log.error("添加缓存失败", e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }

    }

    public String setNX(final String key, final Object value, final int expirationSecond) {
        if (StringUtils.isBlank(key) || value == null) {
            log.error("参数异常,setNX失败");
            return "ERROR";
        }
        String result = "ERROR";
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            result = jedis.set(redisTemplate.getKeySerializer().serialize(key),
                    redisTemplate.getValueSerializer().serialize(value), "NX".getBytes(),
                    "EX".getBytes(), expirationSecond);
        } catch (Exception e) {
            log.error("setNX失败", e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    public Object get(final String key) {
        if (StringUtils.isBlank(key)) {
            log.error("参数异常,获取缓存失败");
            return null;
        }
        Jedis jedis = null;
        Object result = null;
        try {
            jedis = jedisPool.getResource();
            result = redisTemplate.getValueSerializer()
                    .deserialize(jedis.get(redisTemplate.getKeySerializer().serialize(key)));
        } catch (Exception e) {
            log.error("获取缓存失败", e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    public Object incr(final String key) {
        if (StringUtils.isBlank(key)) {
            log.error("参数异常,获取序号失败");
            return null;
        }
        Jedis jedis = null;
        Object result = null;
        try {
            jedis = jedisPool.getResource();
            Long incr = jedis.incr(key);
        } catch (Exception e) {
            log.error("获取序号失败", e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    public Long queryTtl(final String key) {
        if (StringUtils.isBlank(key)) {
            log.error("参数异常,获取缓存过期时间失败");
            return null;
        }
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = jedisPool.getResource();
            result = jedis.ttl(redisTemplate.getKeySerializer().serialize(key));
        } catch (Exception e) {
            log.error("获取缓存过期时间失败", e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    public void setTtl(final String key, int ttl) {
        if (StringUtils.isBlank(key)) {
            log.error("参数异常,设置缓存过期时间失败");
        }
        Jedis jedis = null;

        try {
            jedis = jedisPool.getResource();
            jedis.expire(redisTemplate.getKeySerializer().serialize(key), ttl);
        } catch (Exception e) {
            log.error("设置缓存过期时间失败", e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    public void del(final String key) {
        if (StringUtils.isBlank(key)) {
            log.error("参数异常,删除缓存失败");
            return;
        }
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.del(redisTemplate.getKeySerializer().serialize(key));
        } catch (Exception e) {
            log.error("删除缓存失败", e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    public boolean acquire(final String key, final int limit, final int intervalSecond) {
        if (StringUtils.isBlank(key) || limit <= 0) {
            log.error("参数异常,限流失败");
            return false;
        }
        List<String> argv = Arrays.asList(String.valueOf(limit), String.valueOf(intervalSecond));
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            Object result = jedis.eval(rateLimitScript, Collections.singletonList(key), argv);
            return 1 == (long) result;
        } catch (Exception e) {
            log.error("限流脚本执行出错", e);
            return true;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    public Object incr(final String key, final Integer step, final Integer intervalSecond) {
        if(StringUtils.isBlank(key) || intervalSecond == null || step == null) {
            throw new IllegalArgumentException("计数参数错误");
        }
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            return jedis.eval(countScript, Collections.singletonList(key), Arrays.asList(intervalSecond.toString(), step.toString()));
        } catch (Exception e) {
            log.error("计数脚本执行出错", e);
            throw e;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }
}
