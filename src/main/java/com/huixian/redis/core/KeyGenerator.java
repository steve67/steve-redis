/**
 * HuiXian Inc. Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.huixian.redis.core;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 缓存主键生成器
 *
 * @author LiuYi
 * @version $Id: CacheKeyGenerator.java, v 0.1 2016年3月20日 下午9:05:29 LiuYi Exp $
 */
@Component
@Slf4j
public class KeyGenerator {

    @Value("${spring.application.name}")
    private String applicationName;

    /**
     * 生成缓存主键
     */
    public String generateCacheKey(String group) {

        StringBuilder paramStr = new StringBuilder();
        // 项目名
        paramStr.append(applicationName);
        paramStr.append(":");
        paramStr.append("cache");
        paramStr.append(":");
        // key group
        paramStr.append(group);
        return paramStr.toString();
    }

    /**
     * 生成缓存主键
     */
    public String generateCacheKey(String group, Object[] params) {

        StringBuilder paramStr = new StringBuilder();
        // 项目名
        paramStr.append(applicationName);
        paramStr.append(":");
        paramStr.append("cache");
        paramStr.append(":");
        // key group
        paramStr.append(group);
        // 业务
        if (params != null && params.length > 0) {
            paramStr.append(":");
            int index = 0;
            for (Object param : params) {
                if (index > 0) {
                    paramStr.append("|");
                }
                if (param instanceof Object[]) {
                    paramStr.append(toString((Object[]) param));
                } else {
                    paramStr.append(param);
                }
                index++;
            }
        }

        return paramStr.toString();
    }

    /**
     * 生成缓存主键
     */
    public String generateCacheKey(String group, String key) {
        if (StringUtils.isBlank(key)) {
            return null;
        }
        StringBuilder paramStr = new StringBuilder();
        // 项目名
        paramStr.append(applicationName);
        paramStr.append(":");
        paramStr.append("cache");
        paramStr.append(":");
        // key group
        paramStr.append(group);
        paramStr.append(":");
        paramStr.append(key);
        return paramStr.toString();
    }

    /**
     * 生成分布式锁主键
     */
    public String generateLockKey(String group, String key) {
        if (StringUtils.isBlank(key)) {
            return null;
        }
        StringBuilder paramStr = new StringBuilder();
        // 项目名
        paramStr.append(applicationName);
        paramStr.append(":");
        paramStr.append("lock");
        paramStr.append(":");
        // key group
        paramStr.append(group);
        paramStr.append(":");
        paramStr.append(key);
        return paramStr.toString();
    }


    /**
     * 生成限流主键
     */
    public String generateRateLimitKey(String className, String methodName) {
        StringBuilder paramStr = new StringBuilder();
        // 项目名
        paramStr.append(applicationName);
        paramStr.append(":");
        paramStr.append("limit");
        paramStr.append(":");
        // key group
        paramStr.append(className);
        paramStr.append(":");
        paramStr.append(methodName);
        return paramStr.toString();
    }


    /**
     * 生成限流主键
     */
    public String generateRateLimitKey(String className, String methodName, String key) {
        if (StringUtils.isBlank(key)) {
            return null;
        }
        StringBuilder paramStr = new StringBuilder();
        // 项目名
        paramStr.append(applicationName);
        paramStr.append(":");
        // 限流分组
        paramStr.append("limit");
        paramStr.append(":");
        paramStr.append(className);
        paramStr.append(":");
        paramStr.append(methodName);
        paramStr.append(":");
        paramStr.append(key);
        return paramStr.toString();
    }

    /**
     * 生成限流主键
     */
    public String generateForeverRateLimitKey(String group, String key) {
        if (StringUtils.isBlank(key)) {
            return null;
        }
        StringBuilder paramStr = new StringBuilder();
        // 项目名
        paramStr.append(applicationName);
        paramStr.append(":");
        // 限流分组
        paramStr.append("limit");
        paramStr.append(":");
        paramStr.append("forever");
        paramStr.append(":");
        paramStr.append(group);
        paramStr.append(":");
        paramStr.append(key);
        return paramStr.toString();
    }

    /**
     * 打印对象
     */
    private String toString(Object[] array) {
        StringBuilder result = new StringBuilder("[");
        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                result.append(",");
            }
            result.append(array[i] == null ? "" : array[i]);
        }
        result.append("]");

        return result.toString();
    }

}
