package com.huixian.redis.cache.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 缓存删除注解 create at  2018/08/23 14:26
 *
 * @author yanggang
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface CacheDeletes {

    CacheDelete[] value();
}
