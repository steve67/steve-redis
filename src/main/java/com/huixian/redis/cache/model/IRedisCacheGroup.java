package com.huixian.redis.cache.model;

/**
 * create at  2018/08/23 15:57
 *
 * @author yanggang
 */
public interface IRedisCacheGroup {

    String getGroup();

}
