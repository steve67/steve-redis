/**
 * HuiXian Inc. Copyright (c) 2004-2016 All Rights Reserved.
 */
package com.huixian.redis.cache.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 缓存添加注解
 *
 * @author LiuYi
 * @version $Id: Cache.java, v 0.1 2016年3月20日 下午8:14:54 LiuYi Exp $
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Cache {

    /**
     * 缓存分组
     *
     * @return
     */
    String group();

    /**
     * spring El 表达式
     */
    String elValue() default "";

    /**
     * 缓存过期（单位：秒）
     *
     * @return
     */
    int expiredSecond();

    /**
     * 支持缓存放入NULL值
     *
     * @return
     */
    boolean supportNull() default true;
}
