/**
 * HuiXian Inc. Copyright (c) 2004-2016 All Rights Reserved.
 */
package com.huixian.redis.cache.model;

import java.io.Serializable;

/**
 * 空对象，当需要缓存null值时用此对象替代
 *
 * @author LiuYi
 * @version $Id: NullObject.java, v 0.1 2016年3月20日 下午8:28:34 LiuYi Exp $
 */
public class NullObject implements Serializable {

    /**  */
    private static final long serialVersionUID = 5232597540127380031L;

    /**
     * @see Object#toString()
     */
    @Override
    public String toString() {
        return "null";
    }
}
