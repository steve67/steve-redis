package com.huixian.redis.cache.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 缓存删除注解 create at  2018/08/23 14:26
 *
 * @author yanggang
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Repeatable(CacheDeletes.class)
public @interface CacheDelete {

    /**
     * 缓存分组
     */
    String group();

    /**
     * spring El 表达式
     */
    String elValue() default "";
}
