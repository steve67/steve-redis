/**
 * HuiXian Inc. Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.huixian.redis.cache.core;


import com.huixian.redis.cache.model.Cache;
import com.huixian.redis.cache.model.CacheDelete;
import com.huixian.redis.cache.model.CacheDeletes;
import com.huixian.redis.cache.model.NullObject;
import com.huixian.redis.core.KeyGenerator;
import com.huixian.redis.core.RedisService;
import java.lang.reflect.Method;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.annotation.Order;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

/**
 * 注解缓存拦截器
 *
 * @author yanggang
 */
@Aspect
@Component
@Slf4j
@Order(2)
public class CacheAnnotationInterceptor {

    @Autowired
    private RedisService redisService;

    @Autowired
    private KeyGenerator keyGenerator;

    @Autowired
    private ExpressionParser expressionParser;

    @Autowired
    private LocalVariableTableParameterNameDiscoverer discoverer;

    /**
     * 定义拦截规则
     */
    @Pointcut("execution (* com.huixian.*.service..impl.*.*(..))"
            + "|| execution(* com.huixian.*.biz.process..impl.*.*(..))"
            + "|| execution(* com.huixian.*.dubbo.provider..*.*(..))")
    public void annotationPointcut() {
    }


    @Around("annotationPointcut()")
    public Object interceptor(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Method method = getMethod(proceedingJoinPoint);
        //判断是否配置了缓存添加注解，如果没有配置，则不进行缓存相关处理
        Object result;
        if (method.isAnnotationPresent(Cache.class)) {
            result = cachePut(proceedingJoinPoint, method);
        } else {
            result = proceedingJoinPoint.proceed();
        }

        // 判断是否配置了缓存删除注解，如果没有配置，则不进行缓存相关处理
        if (method.isAnnotationPresent(CacheDeletes.class)) {
            CacheDeletes cacheDeletes = method.getAnnotation(CacheDeletes.class);
            for (CacheDelete cacheDelete : cacheDeletes.value()) {
                cacheDelete(proceedingJoinPoint, method, cacheDelete);
            }
        } else if (method.isAnnotationPresent(CacheDelete.class)) {
            cacheDelete(proceedingJoinPoint, method, method.getAnnotation(CacheDelete.class));
        }
        return result;
    }

    private void cacheDelete(ProceedingJoinPoint proceedingJoinPoint, Method method,
            CacheDelete cacheDelete) {
        String key;
        try {

            if (StringUtils.isBlank(cacheDelete.elValue())) {
                key = keyGenerator
                        .generateCacheKey(cacheDelete.group());
            } else {
                key = keyGenerator.generateCacheKey(cacheDelete.group(),
                        parseKey(cacheDelete.elValue(), method, proceedingJoinPoint.getArgs()));
            }
        } catch (Exception e) {
            log.error("缓存key生成失败", e);
            return;
        }
        redisService.del(key);
    }

    private Object cachePut(ProceedingJoinPoint proceedingJoinPoint, Method method)
            throws Throwable {
        //获取缓存配置信息，根据方法入参生成key去查询缓存容器，看是否命中缓存
        Cache cacheAnnotation = method.getAnnotation(Cache.class);
        // 缓存过期时间小于等于0 则不设置缓存
        if (cacheAnnotation.expiredSecond() <= 0) {
            log.error("过期时间不可小于0");
            return proceedingJoinPoint.proceed();
        }
        String key = null;
        // elValue为空，则使用 group + 入参生成key
        try {
            if (StringUtils.isBlank(cacheAnnotation.elValue())) {
                key = keyGenerator
                        .generateCacheKey(cacheAnnotation.group(), proceedingJoinPoint.getArgs());
            }
            // elValue不为空，则使用 group + el表达式生成key
            else {
                key = keyGenerator.generateCacheKey(cacheAnnotation.group(),
                        parseKey(cacheAnnotation.elValue(), method, proceedingJoinPoint.getArgs()));
            }
        } catch (Exception e) {
            log.error("缓存key生成失败", e);
            return proceedingJoinPoint.proceed();
        }

        Object value = null;
        try {
            value = redisService.get(key);
            //由于null无法设置进缓存，因此转换为NullObject对象作为缓存值，取出时再转换回来
            if (value instanceof NullObject) {
                return null;
            }
        } catch (Exception e) {
            log.error("获取缓存发生异常。cacheKey:" + key, e);
        }

        //如果查询缓存未命中，则调用方法本体逻辑进行处理
        if (value == null) {
            value = proceedingJoinPoint.proceed();

            //对方法本体逻辑获取到的结果设置进缓存
            try {
                if (value != null) {
                    redisService.setEX(key, value, cacheAnnotation.expiredSecond());
                } else {
                    if (cacheAnnotation.supportNull()) {
                        redisService.setEX(key, new NullObject(), cacheAnnotation.expiredSecond());
                    }
                }
            } catch (Exception e) {
                log.error("设置缓存发生异常。cacheKey:" + key, e);
            }
        }

        return value;
    }

    /**
     * 获取被拦截方法对象 <p> MethodSignature.getMethod() 获取的是顶层接口或者父类的方法对象 而缓存的注解在实现类的方法上
     * 所以应该使用反射获取当前对象的方法对象
     */
    public Method getMethod(ProceedingJoinPoint pjp) throws Throwable {
        //获取参数的类型
        MethodSignature methodSignature = (MethodSignature) pjp.getSignature();
        return pjp.getTarget().getClass()
                .getMethod(pjp.getSignature().getName(), methodSignature.getParameterTypes());
    }

    /**
     * 获取缓存的key key 定义在注解上，支持SPEL表达式
     */
    private String parseKey(String key, Method method, Object[] args) {

        //获取被拦截方法参数名列表(使用Spring支持类库)
        String[] paraNameArr = discoverer.getParameterNames(method);
        if (paraNameArr == null || paraNameArr.length <= 0) {
            return key;
        }
        //使用SPEL进行key的解析
        //SPEL上下文
        StandardEvaluationContext context = new StandardEvaluationContext();
        //把方法参数放入SPEL上下文中
        for (int i = 0; i < paraNameArr.length; i++) {
            context.setVariable(paraNameArr[i], args[i]);
        }
        return expressionParser.parseExpression(key).getValue(context, String.class);
    }
}
