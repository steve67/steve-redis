package com.huixian.redis.cache.core;

import com.huixian.redis.cache.model.IRedisCacheGroup;
import com.huixian.redis.cache.model.NullObject;
import com.huixian.redis.core.KeyGenerator;
import com.huixian.redis.core.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * create at  2018/08/23 16:12
 *
 * @author yanggang
 */
@Component
@Slf4j
public class RedisCacheManager {

    @Autowired
    private KeyGenerator keyGenerator;

    @Autowired
    private RedisService redisService;

    /**
     * 设置缓存
     * @param group
     * @param key
     * @param value
     * @param expiredSecond
     */
    public void put(IRedisCacheGroup group, String key, Object value, int expiredSecond) {
        // 参数异常 直接返回
        if (group == null || StringUtils.isBlank(key) || expiredSecond <= 0) {
            log.error("参数异常，缓存添加失败,group:{},key:{}",group,key);
            return;
        }
        // 防止缓存穿透处理
        if (value == null) {
            value = new NullObject();
        }
        redisService
                .setEX(keyGenerator.generateCacheKey(group.getGroup(), key), value, expiredSecond);
    }

    /**
     * 覆盖缓存（原缓存不存在则无法覆盖）
     * @param group
     * @param key
     * @param value
     */
    public void cover(IRedisCacheGroup group, String key, Object value) {
        // 参数异常 直接返回
        if (group == null || StringUtils.isBlank(key)) {
            log.error("参数异常，缓存添加失败,group:{},key:{}",group,key);
            return;
        }
        // 防止缓存穿透处理
        if (value == null) {
            value = new NullObject();
        }
        String cacheKey = keyGenerator.generateCacheKey(group.getGroup(), key);
        if (redisService.get(cacheKey) == null) {
            log.error("原缓存不存在，缓存覆盖失败,group:{},key:{}",group,key);
        }else {
            Long ttl = redisService.queryTtl(cacheKey);
            redisService.set(cacheKey, value);
            redisService.setTtl(cacheKey,ttl.intValue());
        }
    }

    /**
     *
     * @param group
     * @param key
     */
    public void delete(IRedisCacheGroup group, String key) {
        if (group == null || StringUtils.isBlank(key)) {
            log.error("参数异常，缓存删除失败,group:{},key:{}",group,key);
            return;
        }
        redisService.del(keyGenerator.generateCacheKey(group.getGroup(), key));
    }

    /**
     *
     * @param group
     * @param key
     * @return
     */
    public Object get(IRedisCacheGroup group, String key) {
        if (group == null || StringUtils.isBlank(key)) {
            log.error("参数异常，缓存获取失败,group:{},key:{}",group,key);
            return null;
        }
        Object result = redisService.get(keyGenerator.generateCacheKey(group.getGroup(), key));
        if (result instanceof NullObject) {
            return null;
        }
        return result;
    }

    /**
     * 获取计数值
     * @param group 缓存组
     * @param key 缓存键
     * @param step 步长
     * @param ttl 超时时间 当key不存在的时候设置超时时间,之后不设置
     * @return 自增序列值
     */
    public Integer autoIncrement(IRedisCacheGroup group, String key, Integer step, Integer ttl) {
        if (group == null || StringUtils.isBlank(key)) {
            log.error("参数异常，缓存获取失败,group:{},key:{}",group,key);
            return null;
        }
        Object result = redisService.incr(keyGenerator.generateCacheKey(group.getGroup(), key), step, ttl);
        if (result instanceof NullObject) {
            return null;
        }
        return Integer.valueOf(result.toString());
    }

    /**
     * 获取计数值 步长：1
     * @param group 缓存组
     * @param key 缓存键
     * @param ttl 超时时间
     * @return 自增序列值
     */
    public Integer autoIncrement(IRedisCacheGroup group, String key, Integer ttl) {
        return autoIncrement(group, key, 1, ttl);
    }
}
