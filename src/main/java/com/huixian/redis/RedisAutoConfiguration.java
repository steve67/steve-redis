package com.huixian.redis;

import com.huixian.redis.cache.core.CacheAnnotationInterceptor;
import com.huixian.redis.cache.core.RedisCacheManager;
import com.huixian.redis.core.KeyGenerator;
import com.huixian.redis.core.RedisConfig;
import com.huixian.redis.core.RedisService;
import com.huixian.redis.limit.core.RateLimitAnnotationInterceptor;
import com.huixian.redis.limit.core.RateLimitManager;
import com.huixian.redis.lock.core.RedisLockManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * create at  2018/08/22 10:53
 *
 * @author yanggang
 */
@Configuration
@Import(value = {CacheAnnotationInterceptor.class
        , KeyGenerator.class
        , RedisConfig.class
        , RedisService.class
        , RedisCacheManager.class
        , RedisLockManager.class
        , RateLimitManager.class
        , RateLimitAnnotationInterceptor.class})
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class RedisAutoConfiguration {

    @Bean
    public ExpressionParser expressionParser() {
        return new SpelExpressionParser();
    }

    @Bean
    public LocalVariableTableParameterNameDiscoverer localVariableTableParameterNameDiscoverer() {
        return new LocalVariableTableParameterNameDiscoverer();
    }

}
