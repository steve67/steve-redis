package com.huixian.redis.limit.model;

/**
 * Description:   .
 *
 * @author : yanggang
 * @date : Created in 2018-10-29 0029 14:23
 */
public interface RateLimitGroup {
    String getGroup();
}
