package com.huixian.redis.limit.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 限流注解 create at  2018/08/24 10:32
 *
 * @author yanggang
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Repeatable(RateLimits.class)
public @interface RateLimit {

    /**
     * springEL 表达式
     */
    String elValue() default "";

    /**
     * 限制次数
     */
    int limitTimes();

    /**
     * 间隔时间
     */
    int intervalSecond();

}
