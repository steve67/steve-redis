package com.huixian.redis.limit.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description:   .
 *
 * @author : yanggang
 * @date : Created in 2018-10-29 0029 14:31
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface RateLimits {
    RateLimit[] value();
}
