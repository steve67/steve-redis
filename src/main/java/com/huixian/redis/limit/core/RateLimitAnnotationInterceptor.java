package com.huixian.redis.limit.core;

import com.huixian.common2.model.CommonError;
import com.huixian.common2.model.SystemException;
import com.huixian.redis.core.KeyGenerator;
import com.huixian.redis.core.RedisService;
import com.huixian.redis.limit.model.RateLimit;
import java.lang.reflect.Method;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.annotation.Order;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

/**
 * 限流注解拦截器 create at  2018/08/24 10:36
 *
 * @author yanggang
 */
@Slf4j
@Aspect
@Component
@Order(1)
public class RateLimitAnnotationInterceptor {

    @Autowired
    private RedisService redisService;

    @Autowired
    private KeyGenerator keyGenerator;

    @Autowired
    private ExpressionParser expressionParser;

    @Autowired
    private LocalVariableTableParameterNameDiscoverer discoverer;

    /**
     * 定义拦截规则
     */
    @Pointcut("execution (* com.huixian.*.controller..*.*(..))"
            + "|| execution(* com.huixian.*.service..impl.*.*(..))"
            + "|| execution(* com.huixian.*.biz.process..impl.*.*(..))")
    public void annotationPointcut() {
    }

    @Around("annotationPointcut()")
    public Object interceptor(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Method method = getMethod(proceedingJoinPoint);
        if (method.isAnnotationPresent(RateLimit.class)) {
            doLimit(method, proceedingJoinPoint);
            return proceedingJoinPoint.proceed();
        } else {
            return proceedingJoinPoint.proceed();
        }
    }

    private void doLimit(Method method, ProceedingJoinPoint proceedingJoinPoint) {
        RateLimit rateLimit = method.getDeclaredAnnotation(RateLimit.class);
        String methodName = method.getName();
        String className = proceedingJoinPoint.getTarget().getClass().getName();
        String key;
        try {
            if (StringUtils.isBlank(rateLimit.elValue())) {
                key = keyGenerator.generateRateLimitKey(className, methodName);
            } else {
                key = keyGenerator.generateRateLimitKey(className, methodName,
                        parseKey(rateLimit.elValue(), method, proceedingJoinPoint.getArgs()));
            }
        } catch (Exception e) {
            log.error("缓存key生成失败，限流失败", e);
            return;
        }
        boolean acquire = redisService
                .acquire(key, rateLimit.limitTimes(), rateLimit.intervalSecond());
        if (!acquire) {
            throw new SystemException(CommonError.RATE_LIMIT);
        }
    }

    /**
     * 获取被拦截方法对象 <p> MethodSignature.getMethod() 获取的是顶层接口或者父类的方法对象 而缓存的注解在实现类的方法上
     * 所以应该使用反射获取当前对象的方法对象
     */
    public Method getMethod(ProceedingJoinPoint pjp) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) pjp.getSignature();
        return pjp.getTarget().getClass()
                .getMethod(pjp.getSignature().getName(), methodSignature.getParameterTypes());
    }

    /**
     * 获取缓存的key key 定义在注解上，支持SPEL表达式
     */
    private String parseKey(String key, Method method, Object[] args) {

        //获取被拦截方法参数名列表(使用Spring支持类库)
        String[] paraNameArr = discoverer.getParameterNames(method);
        if (paraNameArr == null || paraNameArr.length <= 0) {
            return key;
        }
        //使用SPEL进行key的解析
        //SPEL上下文
        StandardEvaluationContext context = new StandardEvaluationContext();
        //把方法参数放入SPEL上下文中
        for (int i = 0; i < paraNameArr.length; i++) {
            context.setVariable(paraNameArr[i], args[i]);
        }
        return expressionParser.parseExpression(key).getValue(context, String.class);
    }

}
