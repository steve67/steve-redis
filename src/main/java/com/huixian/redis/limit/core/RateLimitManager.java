package com.huixian.redis.limit.core;

import com.huixian.common2.model.CommonError;
import com.huixian.common2.model.SystemException;
import com.huixian.redis.core.KeyGenerator;
import com.huixian.redis.core.RedisService;
import com.huixian.redis.limit.model.RateLimitGroup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * create at  2018/08/28 17:24
 *
 * @author yanggang
 */
@Component
@Slf4j
public class RateLimitManager {

    @Autowired
    private RedisService redisService;

    @Autowired
    private KeyGenerator keyGenerator;

    /**
     * 限流
     *
     * @param group 限流group
     * @param value 限流值
     * @param limitTimes 限制次数
     * @param intervalSecond 间隔时间
     * @return 是否触发限流
     */
    public void limit(RateLimitGroup group, String value, int limitTimes, int intervalSecond) {

        if (group == null || limitTimes <= 0 || intervalSecond <= 0) {
            log.error("参数错误,限流失败,group:{},value:{}",group,value);
            return;
        }

        if (!redisService
                .acquire(keyGenerator.generateRateLimitKey(group.getGroup(), value), limitTimes,
                        intervalSecond)) {
            throw new SystemException(CommonError.RATE_LIMIT);
        }
    }

    public void limitForever(RateLimitGroup group, String value, int limitTimes) {
        if (group == null || limitTimes <= 0) {
            log.error("参数错误,限流失败,group:{},value:{}",group,value);
            return;
        }

        if (!redisService
                .acquire(keyGenerator.generateForeverRateLimitKey(group.getGroup(), value), limitTimes,
                        -1)) {
            throw new SystemException(CommonError.RATE_LIMIT);
        }
    }

}
