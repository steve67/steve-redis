package com.huixian.redis.lock.model;

/**
 * create at  2018/08/23 18:13
 *
 * @author yanggang
 */
public interface IRedisLockGroup {

    String getGroup();

    /**
     * 锁时间,默认60秒
     * @return
     */
    default Integer getLockSecond() {
        return 60;
    }
}
