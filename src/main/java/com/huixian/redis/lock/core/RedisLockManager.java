package com.huixian.redis.lock.core;

import com.huixian.redis.core.KeyGenerator;
import com.huixian.redis.core.RedisService;
import com.huixian.redis.lock.model.IRedisLockGroup;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * create at  2018/08/23 18:11
 *
 * @author yanggang
 */
@Service
@Slf4j
public class RedisLockManager {

    private static final String LOCK_SUCCESS = "OK";

    @Autowired
    private RedisService redisService;

    @Autowired
    private KeyGenerator keyGenerator;

    public boolean lock(IRedisLockGroup group, String key, int lockSecond) {
        if (group == null || StringUtils.isBlank(group.getGroup()) || StringUtils.isBlank(key)
                || lockSecond <= 0) {
            log.error("参数错误，加锁失败,group:{},key:{}", group, key);
            return false;
        }
        String generateLockKey = keyGenerator.generateLockKey(group.getGroup(), key);

        String result = redisService.setNX(generateLockKey, key, lockSecond);
        if (LOCK_SUCCESS.equalsIgnoreCase(result)) {
            return true;
        }
        return false;
    }

    public void unLock(IRedisLockGroup group, String key) {
        if (group == null || StringUtils.isBlank(group.getGroup()) || StringUtils.isBlank(key)) {
            log.error("参数错误，解锁失败,group:{},key:{}", group, key);
            return;
        }
        redisService.del(keyGenerator.generateLockKey(group.getGroup(), key));
    }

    public boolean lock(IRedisLockGroup group, String... keys) {
        if (group == null || group.getLockSecond() <= 0 || keys == null || keys.length == 0) {
            log.error("参数错误，加锁失败,group:{},key:{}", group, keys);
            return false;
        }
        String keyStr = StringUtils.join(keys, '|');
        return lock(group, keyStr, group.getLockSecond());
    }

    public void unLock(IRedisLockGroup group, String... keys) {
        if (group == null || StringUtils.isBlank(group.getGroup()) || keys == null
                || keys.length == 0) {
            log.error("参数错误，解锁失败,group:{},key:{}", group, keys);
            return;
        }
        String keyStr = StringUtils.join(keys, '|');
        redisService.del(keyGenerator.generateLockKey(group.getGroup(), keyStr));
    }
}
